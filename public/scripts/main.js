var current = {
};
var overall = {
    "mistakes":0,
    "hints":0,
    "verseCount":0
};
function isNextTerm(term){
    var solution = current.solution;
    if(solution.length>0){
        solution += " ";
    }
    solution+=term;
    return (solution==current.actual.substring(0,solution.length));
}
function addTerm(index){
    if(current.solution.length>0){
        current.solution += " ";
    }
    current.solution+=current.scrambled[index];
    current.scrambled.splice(index,1);
    if(current.scrambled.length>0){
        updateCurrent(false);
    }else{
        overall.verseCount++;
        newVerse();
    }
}
function addMistake(){
    overall.mistakes++;
    updateCurrent(false);
}
function showHint(){
    overall.hints++;
    updateCurrent(true);
}
function updateCurrent(hint){
    var content="";
    content+="<h3>";
    content += current.reference;
    content+="</h3>";
    content+="<p><i>Solution: </i>" + current.solution + "</p>";
    content+="<p><i>Terms:</i>"

    for(var i in current.scrambled) {
        var term = current.scrambled[i];
        if(hint && isNextTerm(term)) {
            content += "<button type=\"button\" class=\"btn btn-outline-success\"";
        } else {
            content += "<button type=\"button\" class=\"btn btn-outline-primary\"";
        }
        if(isNextTerm(term)){
            content += " onclick=\"addTerm("+i+")\" ";
        }else{
            content += " onclick=\"addMistake()\" ";
        }
        content += ">";
        content += term;
        content += "</button>";
    }
    content+="</p>";
    content+="<p><button type=\"button\" class=\"btn btn-outline-warning\" onclick=\"showHint()\">HINT</button></p>"
    content+="<p><i>Mistakes:</i> " + overall.mistakes + " | <i>Hints:</i> " + overall.hints + " | <i>Verses:</i> "+overall.verseCount+"</p>";
    document.getElementById("mainDiv").innerHTML=content;
}
function newVerse(){
    current.verseIndex = Math.floor(Math.random() * verses.length);
    var verse = verses[current.verseIndex];
    current.reference = verse.reference;
    current.actual = verse.text;
    current.solution = "";
    current.scrambled = verse.text.split(" ").sort(  function(a, b) {
        if (a.toLowerCase() < b.toLowerCase()) return -1;
        if (a.toLowerCase() > b.toLowerCase()) return 1;
        return 0;
      });
    updateCurrent(false);
}
function main(){
    newVerse();
}