var verses = [
    {
        "tags":["Holiness"],
        "reference":"Leviticus 19:2 ESV",
        "text":"Speak to all the congregation of the people of Israel and say to them, You shall be holy, for I the LORD your God am holy."
    },
    {
        "tags":["Fallen"],
        "reference":"Ecclesiastes 7:10 ESV",
        "text":"Surely there is not a righteous man on earth who does good and never sins."
    },
    {
        "tags":["Fallen"],
        "reference":"Proverbs 20:9 ESV",
        "text":"Who can say, \"I have made my heart pure; I am clean from my sin\"?"
    },
    {
        "tags":["Fallen"],
        "reference":"Psalm 143:2 ESV",
        "text":"Enter not into judgment with your servant, for no one living is righteous before you."
    },
    {
        "tags":["Fallen"],
        "reference":"Romans 3:23 ESV",
        "text":"for all have sinned and fall short of the glory of God,"
    },
    {
        "tags":["Holiness"],
        "reference":"Matthew 5:48 ESV",
        "text":"You therefore must be perfect, as your heavenly Father is perfect."
    },
    {
        "tags":["Fallen"],
        "reference":"Psalm 49:7 ESV",
        "text":"Truly no man can ransom another, or give to God the price of his life,"
    },
    {
        "tags":["Fallen"],
        "reference":"Isaiah 64:6",
        "text":"We have all become like one who is unclean, and all our righteous deeds are like a polluted garment. We all fade like a leaf, and our iniquities, like the wind, take us away."
    },
    {
        "tags":["Repentence"],
        "reference":"Luke 15:7",
        "text":"Just so, I tell you, there will be more joy in heaven over one sinner who repents than over ninety-nine righteous persons who need no repentance."
    },
    {
        "tags":["Fallen"],
        "reference":"Romans 3:9",
        "text":"What then? Are we Jews any better off? No, not at all. For we have already charged that all, both Jews and Greeks, are under sin,"
    },
    {
        "tags":["Fallen"],
        "reference":"Romans 3:10",
        "text":"as it is written: \"None is righteous, no, not one;"
    },
    {
        "tags":["Fallen"],
        "reference":"Romans 3:11",
        "text":"no one understands; no one seeks for God."
    },
    {
        "tags":["Fallen"],
        "reference":"Romans 3:12",
        "text":"All have turned aside; together they have become worthless; no one does good, not even one.\""
    },
    {
        "tags":["Repentence"],
        "reference":"Acts 3:19",
        "text":"Repent therefore, and turn back, that your sins may be blotted out,"
    },
];
